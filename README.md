# Radiogenomics MAE: a case study

## MAE_case_study.R

Script used to create a MultiAssayExperiment object that integrates biological 'omics data and radiomic data (features). 

Biological 'omic data are downloaded by the script through curatedTCGA R package.


## TCGA_Run_2014_91cases_features.csv

Radiomic features downloaded from TCGA Breast Phenotype Research Group Data sets available on TCIA data portal.

## brca-clinicalforwiki.csv

Clinical data downloaded from TCGA Breast Phenotype Research Group Data sets available on TCIA data portal.


### Link to TCGA Breast Phenotype Research Group Data sets: 

https://wiki.cancerimagingarchive.net/display/DOI/TCGA+Breast+Phenotype+Research+Group+Data+sets

## Citation

I am appreciated if you cite our work in your publications:

Zanfardino M, Franzese M, Pane K, Cavaliere C, Monti S, Esposito G, Salvatore M, Aiello M. Bringing radiomics into a multi-omics framework for a comprehensive genotype-phenotype characterization of oncological diseases. J Transl Med. 2019 Oct 7;17(1):337. [doi: 10.1186/s12967-019-2073-2](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6778975/). PMID: 31590671; PMCID: PMC6778975.

## Data Citation

Morris, Elizabeth, Burnside, Elizabeth, Whitman, Gary, Zuley, Margarita, Bonaccio, Ermelinda, Ganott, Marie, … Giger, Maryellen L. (2014). Using Computer-extracted Image Phenotypes from Tumors on Breast MRI to Predict Stage. The Cancer Imaging Archive. http://doi.org/10.7937/K9/TCIA.2014.8SIPIY6G 

## References

1 - Ramos M, Schiffer L, Re A et al. Software for the integration of multiomics experiments in Bioconductor. Cancer Res 2017;77:e39–e42.

2 - Ramos M. curatedTCGA Data: Curated Data From The Cancer Genome Atlas (TCGA) as MultiAssayExperiment Objects. R package version 1.3.5.


## Dependencies

MultiAssayExperiment (http://bioconductor.org/packages/release/bioc/html/MultiAssayExperiment.html)

SummarizedExperiment (https://bioconductor.org/packages/release/bioc/html/SummarizedExperiment.html)

curatedTCGA (http://bioconductor.org/packages/release/data/experiment/html/curatedTCGAData.html)

UpSetR (https://cran.r-project.org/web/packages/UpSetR/index.html)
